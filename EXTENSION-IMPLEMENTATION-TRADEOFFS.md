## Complexity Oriented Architecture Decision Tree: Building GitLab Reusable CI/CD Extensions

### Complexity Heuristic

1. Favor the simplest implementation that will fulfill your requirements balanced by recognizing when your requirements push you toward a more complex implementation. (Make it as simple as possible, but not simpler.)
2. Only incrementally take the next complexity level as required (which means you also need to know the relative complexity levels).
   In this map, complexity is simply indicated by tagging with "Baseline Complexity" and "+XX% Complexity" (where "XX" is a number).

### Enablement Heuristic

1. Teaching materials should start with basic scenarios and move to more sophisticated ones. 
2. However, life-long implementation decisions also should follow the same principles and decision tress whenever possible. 

Cartographing a Complexity Driven Decision Tree helps both the new learner AND the seasoned implementer to apply the best choices to their particular situation and allows documentation of non-adopted and adopted-in-part heuristics.

### Building Reusable GitLab CI/CD Extensions Knowledge

- Depends on:
  - Understanding Declarative Code Languages vs Imperative
  - GitLab's sophisticated "CI YML includes" functionality
  - Optionally: YAML anchors
  - GitLab's ability to create "CI Job Functions" (extends: and jobs starting with a ".")

### Code Sharing Models

- Implementation Model 1: "Grab and Customize" Example Code - each copy is snowflaked and completely seperated from the original example.

- Implementation Model 2: Can be Depended Upon Directly 

  - when constructed to be reused across many CI jobs or projects (baseline complexity). 

- Implementation Model 2.1: Can optionally be centrally release managed in a single location (+80% complexity). 

  Additional Skills & Disciplines: Shared code management => centralized, version managed, non-breaking release management with communications (at least release notes).

  **IMPORTANT NOTE**: This is the most advanced model and it is how AutoDevOps is written - which means the most advanced model is in production use everyday in GitLab.

  > For GitLab Technology Partners, either of the above code sharing models can be done from GitLab.com's "template" folder which makes it available to the entire ecosystem - including self-managed instances - using "include:template:" The rest of the implementation information in this document can be implemented by anyone on any GitLab instance as a form of Developer Enablement.

### Summary of Best Practice Implementation Configurations

1. no container + your inline yaml code (baseline complexity) [Only option for "Grab and Customize" Code Sharing Model]
2. no container + multiple includes of your inline yaml code (+20% complexity)
   - **Working Example Project** [Script Code Libraries In Pure GitLab CI YAML](https://gitlab.com/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/script-code-libraries-in-pure-gitlab-ci-yaml)
3. existing container + runtime installs + your inline yaml code (+25% complexity)
4. custom container backed + your inline yaml code (+70% complexity)
   1. When Required: runtime installs too long, lots of custom code
   2. Additional Skills: container building
   3. Attributes that will require more complex implementation:scope of sharing - 

### To Containerize or Not to Containerize

**Con Heuristic**: Building containers adds another integrated release cycle - avoid it when other concerns don't intercede.

**Pro Heuristic**: Containers version peg everything they contains (more important for centrally managed)
