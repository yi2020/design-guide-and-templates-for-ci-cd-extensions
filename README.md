# A TEMPLATE FOR CI CD EXTENSIONS

A good starting point for building your own.  Includes security specific features and AutoDevOps compatibility.

Please read [EXTENSION-IMPLEMENTATION-TRADEOFFS](./EXTENSION-IMPLEMENTATION-TRADEOFFS.md)
